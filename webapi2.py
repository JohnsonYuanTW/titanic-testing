import os
import errno
import time
import traceback
from flask import Flask, request, jsonify, session, send_from_directory
from flask_session import Session
import redis
from werkzeug.utils import secure_filename
import json
import csv
import numpy as np
import pandas as pd
import pickle
import plotly
import plotly.offline as py
import plotly.graph_objs as go
import plotly.graph_objs.layout

UPLOAD_FOLDER = 'storage'
ALLOWED_EXTENSIONS = set(['csv', 'xls', 'xlsx'])
model_file = 'model/save.pkl'
model_name = ''
rdmf = "" 
classifier = "" 
svm = "" 
k_svm = "" 
knn = "" 
xgb = ""

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_PERMANENT'] = False # True -> pop when browser close
app.config['SESSION_USE_SIGNER'] = False # sign?
app.config['SESSION_KEY_PREFIX'] = 'session:'
app.config['SESSION_REDIS'] = redis.Redis(host='127.0.0.1', port='6379', password='G02iN7F0jaJ41jcr5CjJ9Xp3UQ9SGege') # connecting redis

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = '246cc623b9a47f3e7cb51dae4775703e086f8ccd7045eb96'
app.config.from_object(__name__)

sess = Session()
sess.init_app(app)

################################################################
#                    Session Variable Table                    #
################################################################
# session['userid']  =>  userid                                #
# session['training_data']  =>  csv path                       #
# session['pd_traindata']  =>  csv's dataframe                 #
# session['dependent_variable']  =>  selected target           #
# session['cat_col']  =>  columns recognized as cat. in order  #
# session['model_file']  =>  pkl file path and filename        #
################################################################

# 2018/08/23 @JohnsonYuanTW

# Setting allowed file_exts
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Route for uploading. 
@app.route('/upload', methods=['POST'])
def upload_file():
    message = []  
    global userid 
    file = request.files['file']
    userid = request.form['userid']
    session['userid'] = userid
    # if user does not select file, browser also
    # submit a empty part without filename
    if file.filename == '':
        message = {
            'status':'fail',
            'message':'Empty file. Go to hell.'
        }

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        print(filename)
        #################################################################
        timestamp = str(time.time())
        UPLOAD_FOLDER = 'storage/' + userid + '/mydata/'   
        new_filename = userid + '_' + timestamp + '_' + filename
        session['training_data'] = UPLOAD_FOLDER + new_filename
        if not os.path.exists(os.path.dirname(session['training_data'])):
            try:
                os.makedirs(os.path.dirname(session['training_data']))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        
        app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], new_filename))
        #################################################################
        print('file name:',session['training_data'])

        message = {
            'status':'success',
            'message':'Upload success',
            'data': session['training_data'],
            "origin_filename": filename,
            "filename": new_filename
        }

    # return message
    response = app.response_class(
        response=json.dumps(message),
        mimetype='application/json'
    )
    return response

@app.route('/prep',methods=['GET'])
def prep():
    session['pd_traindata'] = pd.read_csv(session['training_data']) # from /upload
    m = {
        'cols': len(session['pd_traindata'].columns),
        'rows': len(session['pd_traindata']) + 1
    }
    d = []
    for i in session['pd_traindata'].columns:   
        if session['pd_traindata'][i].isnull().sum() != 0:
            na = int(session['pd_traindata'][i].isnull().sum())
        else:
            na = 0
        if session['pd_traindata'][i].dtype == "object":
            st = 1
        else:
            st = 0
        column = { "col":i, "na":na, "str":st }
        d.append(column)
    
    colums = {}
    data = []

    with open(session['training_data']) as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            for fieldname in reader.fieldnames:
                colums.setdefault(fieldname, []).append(row.get(fieldname))

    res = "["
    for key, value in colums.items():
        tmp_pd = pd.DataFrame({key:value})
        valuea = dict(pd.value_counts(tmp_pd.values.flatten()))
        tmp_pd = pd.DataFrame({key:valuea})

        tmp_pd_json = tmp_pd.to_json()
        res += tmp_pd_json + ","
    res = res[:-1]
    res += "]"


    with open('data.json', 'w') as outfile:
        outfile.write(res)

    res = json.loads(res)

    data = {
        'status':'success',
        'message':m,
        'data': d,
        'plotdata' : res
    }

    response = app.response_class(
            response=json.dumps(data),
            mimetype='application/json'
        )
    
    return response

@app.route('/confirm',methods=['GET', 'POST'])
def confirm():
    if request.method == 'POST':
        try:
            json_ = request.json
            d = []
            for key in json_['keep']:
                d.append(key)
            session['pd_traindata'] = session['pd_traindata'][d]

            session['cat_col']=[]
            
            for key in json_['cat']:
                session['pd_traindata'] = session['pd_traindata'][session['pd_traindata'][key].notnull()]  # delete the rows with NAN

                session['pd_traindata'][key] = session['pd_traindata'][key].astype('category')

                dic = dict( enumerate(session['pd_traindata'][key].cat.categories) )
                
                colu = []
                for i in dic.values():
                    colu.append(i) 
                
                cc = { 
                    "col":key,
                    "val":colu 
                }
                session['cat_col'].append(cc)
                
                session['pd_traindata'][key] = session['pd_traindata'][key].astype('category').cat.codes

            for k, v in json_['na'].items():
                if v == 0: # delete
                    session['pd_traindata'] = session['pd_traindata'][session['pd_traindata'][k].notnull()]
                if v == 1: # mean
                    session['pd_traindata'][k].fillna(value=session['pd_traindata'][k].mean(),inplace=True)
                if v == 2: # zero
                    session['pd_traindata'][k].fillna(value=0,inplace=True)

            data = {
                'status':'success',
                'message':'Success preprocessing',
                'data': d
            }

            response = app.response_class(
                response=json.dumps(data),
                mimetype='application/json'
            )

            return response

        except Exception as e:
            print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
            return jsonify({'error': str(e), 'trace': traceback.format_exc()})
    return 'Method Get' 

@app.route('/train', methods=['POST'])
def train():
    
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.model_selection import train_test_split
    
    req_json = ""
    try:
        req_json = request.json

    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})
    
    session['dependent_variable'] = req_json.get("target")
    df = session['pd_traindata']
    print("\n\n",df,"\n\n")

    df_ = pd.get_dummies(df)
    x = df_[df_.columns.difference([session['dependent_variable']])]
    y = df_[session['dependent_variable']]

    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=req_json.get("test_pro"))
    
    # Feature Scaling
    scaler_x = MinMaxScaler((-1,1))
    X_train = scaler_x.fit_transform(X_train)
    X_test = scaler_x.transform(X_test)

    ######################################
    timestamp = str(time.time())
    model_path = 'storage/' + session['userid'] + '/mymodel/'   
    model_name = session['userid'] + '_' + timestamp + '_save.pkl'    
    session['model_file']  = model_path + model_name
    print("\n\nmodel path:  ",session['model_file'])

    if not os.path.exists(os.path.dirname(session['model_file'])):
        try:
            os.makedirs(os.path.dirname(session['model_file']))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    ######################################
    score = ""
    if req_json.get("algo") == 0:
        from sklearn.ensemble import RandomForestClassifier 
        rdmf = RandomForestClassifier(n_estimators=20, criterion='entropy')
        rdmf.fit(X_train, y_train)
        score = rdmf.score(X_test, y_test)

        with open(session['model_file'],'wb') as f:
            pickle.dump(rdmf,f)
        print('Model testing score: %s' % score)
        
    elif req_json.get("algo") == 1:
        from sklearn.linear_model import LogisticRegression
        classifier = LogisticRegression()
        classifier.fit(X_train, y_train)
        score = classifier.score(X_test, y_test)
        with open(session['model_file'],'wb') as f:
            pickle.dump(classifier,f)
        print('Model testing score: %s' % score)

    elif req_json.get("algo") == 2:
        from sklearn.svm import SVC
        svm = SVC(kernel='linear')
        svm.fit(X_train, y_train)
        score = svm.score(X_test, y_test)
        with open(session['model_file'],'wb') as f:
            pickle.dump(svm,f)
        print('Model testing score: %s' % score)

    elif req_json.get("algo") == 3:
        from sklearn.svm import SVC
        k_svm = SVC(kernel='rbf')
        k_svm.fit(X_train, y_train)
        score = k_svm.score(X_test, y_test)
        with open(session['model_file'],'wb') as f:
            pickle.dump(k_svm,f)
        print('Model testing score: %s' % score)

    elif req_json.get("algo") == 4:
        from sklearn.neighbors import KNeighborsClassifier
        knn = KNeighborsClassifier(p=2, n_neighbors=10)
        knn.fit(X_train, y_train)
        score = knn.score(X_test, y_test)
        print('Model testing score: %s' % score)
        with open(session['model_file'],'wb') as f:
            pickle.dump(knn,f)

    elif req_json.get("algo") == 5:
        from xgboost import XGBClassifier  
        xgb = XGBClassifier()
        xgb.fit(X_train, y_train)
        score = xgb.score(X_test, y_test)
        with open(session['model_file'],'wb') as f:
            pickle.dump(xgb,f)
        print('Model testing score: %s' % score)
        
    else:
        return 'No training'
    
    return jsonify({'accuracy': score, 'algo': req_json.get("algo"),'predict_col':session['cat_col'], 'origin_filename':model_name,'filename':model_name,'model_route':session['model_file'] })
 
@app.route('/trainall', methods=['POST'])   
def trainall():
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.model_selection import train_test_split
    req_json = ""
    try:
        req_json = request.json

    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})
    
    session['dependent_variable'] = req_json.get("target")
    df = session['pd_traindata']
    print("\n\n",df,"\n\n")

    df_ = pd.get_dummies(df)
    x = df_[df_.columns.difference([session['dependent_variable']])]
    y = df_[session['dependent_variable']]

    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=req_json.get("test_pro"))

    # Feature Scaling
    scaler_x = MinMaxScaler((-1,1))
    X_train = scaler_x.fit_transform(X_train)
    X_test = scaler_x.transform(X_test)

    ######################################
    global model_name
    timestamp = str(time.time())
    model_path = 'storage/' + session['userid'] + '/mymodel/'   
    model_name = session['userid'] + '_' + timestamp + '_save.pkl'    
    session['model_file']  = model_path + model_name
    print("\n\nmodel path:  ",session['model_file'])

    if not os.path.exists(os.path.dirname(session['model_file'])):
        try:
            os.makedirs(os.path.dirname(session['model_file']))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    ######################################

    from sklearn.ensemble import RandomForestClassifier 
    rdmf = RandomForestClassifier(n_estimators=20, criterion='entropy')     
    rdmf.fit(X_train, y_train)
    rdmf_score = rdmf.score(X_test, y_test)   
    
    from sklearn.linear_model import LogisticRegression
    classifier = LogisticRegression()
    classifier.fit(X_train, y_train)
    lr_score = classifier.score(X_test, y_test)

    from sklearn.svm import SVC
    svm = SVC(kernel='linear')
    svm.fit(X_train, y_train)
    svm_score = svm.score(X_test, y_test)
    
    from sklearn.svm import SVC
    k_svm = SVC(kernel='rbf')
    k_svm.fit(X_train, y_train)
    k_svm_score = k_svm.score(X_test, y_test)

    from sklearn.neighbors import KNeighborsClassifier
    knn = KNeighborsClassifier(p=2, n_neighbors=10)
    knn.fit(X_train, y_train)
    knn_score = knn.score(X_test, y_test)
    
    from xgboost import XGBClassifier  
    xgb = XGBClassifier()
    xgb.fit(X_train, y_train)
    xgb_score = xgb.score(X_test, y_test)
    
    data = [ rdmf_score, lr_score, svm_score, k_svm_score, knn_score, xgb_score]

    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    print(data)

    return response

@app.route('/trainallkeep', methods=['POST'])
def keep():
    try:
        req_json = request.json
        algo = req_json.get("algo")
        sta = "success"
        message = "Success keep model"
        global model_name
        global rdmf  
        global classifier  
        global svm
        global k_svm 
        global knn
        global xgb 

        if algo == 0:
            with open(session['model_file'],'wb') as f:
                pickle.dump(rdmf,f)
        elif algo == 1:
            with open(session['model_file'],'wb') as f:
                pickle.dump(classifier,f)
        elif algo == 2:
            with open(session['model_file'],'wb') as f:
                pickle.dump(svm,f)
        elif algo == 3:
            with open(session['model_file'],'wb') as f:
                pickle.dump(k_svm,f)
        elif algo == 4:
            with open(session['model_file'],'wb') as f:
                pickle.dump(knn,f)
        elif algo == 5:
            with open(session['model_file'],'wb') as f:
                pickle.dump(xgb,f)
        else:
            sta = "fail"
            message = "Fail to save model"


    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})

    data = {
        "status":sta,
        "message" : message,
        "algo":algo,
        "origin_filename": model_name,
        "filename": model_name,
        "model_route": session['model_file']
    }
    
    response = app.response_class(
        response=json.dumps(data),
        mimetype='application/json'
    )
    return response

@app.route('/predict', methods=['POST'])
def predict():
    if model_file:
        try:
            json_ = request.json
            print(type(json_))
            query = pd.get_dummies(pd.DataFrame(json_))

            with open(model_file,'rb') as f:
                clf = pickle.load(f)
                print(clf)

            # https://github.com/amirziai/sklearnflask/issues/3
            # Thanks to @lorenzori
            model_columns = session['pd_traindata'].columns.difference([session['dependent_variable']])
            query = query.reindex(columns=model_columns, fill_value=0)
            prediction = (clf.predict(query)).tolist()

            print({'prediction': prediction})
            return jsonify({'prediction': prediction})

        except Exception as e:
            print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
            return jsonify({'error': str(e), 'trace': traceback.format_exc()})
    else:
        print('train first')
        return 'no model here'
    

@app.route('/getmodel',methods=['POST'])
def get_model():
    try:
        get_mod = request.json
        modelpath = get_mod.get('model_route')
        print(modelpath)
    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})

    if os.path.exists(modelpath):
        return send_from_directory(os.path.dirname(modelpath), os.path.basename(modelpath))
    else:
        message = {
            'status':'fail',
            'message': 'File not exist.'
        }

        # return message
        response = app.response_class(
            response=json.dumps(message),
            mimetype='application/json'
        )
        return response


@app.route('/delmodel',methods=['POST'])
def deletemodel():
    try:
        get = request.json
        modelpath = get.get('model_route')
        print(modelpath)
    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})

    status = ""
    msg = ""
    if os.path.exists(modelpath):
        os.remove(modelpath)
        status = "success"
        msg = "delete success"
    else:
        status = "fail"
        msg = "The file does not exist"


    message = {
        'status':status,
        'message':msg
    }

    # return message
    response = app.response_class(
        response=json.dumps(message),
        mimetype='application/json'
    )
    return response


@app.route('/getdata',methods=['POST'])
def getdata():
    try:
        get_ = request.json
        datapath = get_.get('data_route')
        print(datapath)
    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})

    if os.path.exists(datapath):
        return send_from_directory(os.path.dirname(datapath), os.path.basename(datapath))
    else:
        message = {
            'status':'fail',
            'message': 'File not exist.'
        }

        # return message
        response = app.response_class(
            response=json.dumps(message),
            mimetype='application/json'
        )
        return response


@app.route('/deldata',methods=['POST'])
def deldata():
    try:
        get_ = request.json
        datapath = get_.get('data_route')
        print(datapath)
    except Exception as e:
        print("ERROR",{'error': str(e), 'trace': traceback.format_exc()})
        return jsonify({'error': str(e), 'trace': traceback.format_exc()})

    status = ""
    msg = ""
    if os.path.exists(datapath):
        os.remove(datapath)
        status = "success"
        msg = "delete success"
    else:
        status = "fail"
        msg = "The file does not exist"


    message = {
        'status':status,
        'message':msg
    }

    # return message
    response = app.response_class(
        response=json.dumps(message),
        mimetype='application/json'
    )
    return response


@app.route('/logout',methods=['GET'])
def deleteSession():
    re = "Bye Bye, " + session['userid']
    session.clear()
    return re

@app.route('/',methods=['GET'])
def hello():
    return 'Connection Success!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9999, debug=True)