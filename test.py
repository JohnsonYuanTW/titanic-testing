from flask import Flask, session, redirect, url_for, escape, request
from flask_session import Session

app = Flask(__name__)
app.secret_key = 'any random string'

@app.route('/')
def index():
	if 'username' in session:
		 username = session['username']
		 return 'Hi, ' + username + session['username1'] + '<br>' + \
		 "<b><a href = '/logout'>Log out</a></b>"
	return "Please Login in <br><a href = '/login'></b>" + \
		"log in</b></a>"

@app.route('/login', methods = ['GET', 'POST'])
def login():
	if request.method == 'POST':
		session['username'] = request.form['username']
		session['username1'] = "test"
		session['username2'] = "test"
		return redirect(url_for('index'))
	return '''
	
	<form action = "" method = "post">
		<p><input type = text name = username /></p>
		<p><input type = submit value = Login /></p>
	</form>
	
	'''

@app.route('/logout')
def logout():
	# remove the username from the session if it is there
	session.pop('username', None)
	return redirect(url_for('index'))

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=9999, debug=True)
	session = Session(app)
	session.app.session_interface.db.create_all()