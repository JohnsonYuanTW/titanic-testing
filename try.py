import requests
import json
import time
import urllib.request

tStart = time.time()#計時開始

# prep information
url = 'http://localhost:9999/prep'
contents = urllib.request.urlopen(url).read()

# confirm
url = 'http://localhost:9999/confirm'
with open('test.json') as f:
    data = json.load(f)

def post_call(url, data):
    headers = {'Content-type': 'application/json'}
    data = json.dumps(data)
    res = requests.post(url, data=data, headers=headers)
    return res
 
response = post_call(url, data=data)


#########################################################

# train model
url = 'http://localhost:9999/train'
with open('test2.json') as f:
    data = json.load(f)

response = post_call(url, data=data)
print(response)
# # train all model
# url = 'http://localhost:9999/trainall'
# with open('test4.json') as f:
#     data = json.load(f)

# response = post_call(url, data=data)

##########################################################

tEnd = time.time()#計時結束
# 列印結果print "It cost %f sec" % (tEnd - tStart)#會自動做進位
print("time",tEnd - tStart)#原型長這樣

##########################################################

# url = 'http://localhost:9999/predict'
# with open('test3.json') as f:
#     data = json.load(f)

# response = post_call(url, data=data)